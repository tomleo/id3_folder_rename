#! /usr/bin/python

import os
import re
import glob
import subprocess

from mutagen.easyid3 import EasyID3

path = os.getcwd()
fpath = u"%s/*.mp3" % path
files = glob.glob(fpath)

for fname in files:
    _track = EasyID3(fname)
    track_num = _track.get('tracknumber')[0]
    track_title = re.sub(r'/', '_', _track.get('title')[0])
    if '/' in track_num:
        track_num = track_num.split('/')[0]
    if len(track_num) == 1:
        track_num = "0%s" % track_num
    _valid_fname = u"%s/%s %s.mp3" % (path, track_num, track_title)
    if fname != _valid_fname:
        subprocess.call(["/bin/mv", fname, _valid_fname])

